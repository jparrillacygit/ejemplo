import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'lugares',
    pathMatch: 'full'
  },
  {
    path: 'lugares',
    //loadChildren: () => import('./lugares/lugares.module').then( m => m.LugaresPageModule)
    children:[
      {
        path:"",
        loadChildren: () => import('./lugares/lugares.module').then( m => m.LugaresPageModule)
      },
      {
        path:":placeId",//cuando escribas un numero carga la siguiente ventana
        loadChildren: () => import('./lugares/lugares-detalle/lugares-detalle.module').then( m => m.LugaresDetallePageModule)
      }
    ]
  },
  {
    path: 'nuevo-lugares',
    loadChildren: () => import('./lugares/lugares-add/lugares-add.module').then( m => m.LugaresAddPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
