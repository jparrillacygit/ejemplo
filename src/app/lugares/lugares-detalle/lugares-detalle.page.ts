import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router} from '@angular/router'
import { LugaresService } from '../lugares.service';
import {Place} from '../lugares.model'
import {AlertController} from '@ionic/angular'
//Router permite coger los datos de la url en este caso para sacar el numero
@Component({
  selector: 'app-lugares-detalle',
  templateUrl: './lugares-detalle.page.html',
  styleUrls: ['./lugares-detalle.page.scss'],
})
export class LugaresDetallePage implements OnInit {
  place: Place;
  constructor(private activateRoute:ActivatedRoute, private placeServices: LugaresService,
    private router:Router, private alert:AlertController) { }//desde aqui se instancia las clases genericas

  ngOnInit() {
    this.activateRoute.paramMap.subscribe(
      paramMap =>{
        const recipeID= paramMap.get('placeId')//es el path que le habiamos asignado
        this.place=this.placeServices.getPlace(recipeID)
        console.log(this.place)
      }
    )
  }
  async deletePlace(){
    //await sincroniza la alerta cuando se le pida
    const alerElement=await this.alert.create({//creamos la alerta
      header:'Estas seguro de querer eliminar?',
      message:'Ser cuidadoso',
      buttons:[{
        text:'Cancel',//titulo
        role:'cancel'//
      },
      {
        text:'Delete',
        handler: () =>{//funciona como el onclick de android estudio
          this.placeServices.deletePlace(this.place.id)
    
          this.router.navigate(['/lugares'])//Redirecciona al lugar que pongamos
        }
      }
    ]
    })
    alerElement.present()//presenta el elemento
    
  }

}
