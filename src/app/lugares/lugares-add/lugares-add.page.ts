import { Component, OnInit } from '@angular/core';
import { LugaresService } from '../lugares.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-lugares-add',
  templateUrl: './lugares-add.page.html',
  styleUrls: ['./lugares-add.page.scss'],
})
export class LugaresAddPage implements OnInit {

  constructor(private servicioLugares:LugaresService,private router:Router) { }

  ngOnInit() {
  }
  guardarDatos(title:HTMLInputElement,imageURL:HTMLInputElement){
    this.servicioLugares.addPlace(title.value,imageURL.value)
    this.router.navigate(['/lugares'])
  }
}
