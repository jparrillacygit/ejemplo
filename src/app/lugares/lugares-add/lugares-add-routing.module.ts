import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LugaresAddPage } from './lugares-add.page';

const routes: Routes = [
  {
    path: '',
    component: LugaresAddPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LugaresAddPageRoutingModule {}
