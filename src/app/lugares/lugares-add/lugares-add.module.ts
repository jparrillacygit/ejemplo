import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LugaresAddPageRoutingModule } from './lugares-add-routing.module';

import { LugaresAddPage } from './lugares-add.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LugaresAddPageRoutingModule
  ],
  declarations: [LugaresAddPage]
})
export class LugaresAddPageModule {}
