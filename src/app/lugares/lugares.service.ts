import { Injectable } from '@angular/core';
import {Place} from './lugares.model'
@Injectable({
  providedIn: 'root'
})
export class LugaresService {
  private places: Place[] = [
    {
      id:'1',
      title:'Eiffel Tower',
      imagenURL:'https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Tour_eiffel_at_sunrise_from_the_trocadero.jpg/413px-Tour_eiffel_at_sunrise_from_the_trocadero.jpg',
      coments: ['AWESOME','Incredibolt']
    },
    {
      id:'2',
      title:'PLAZA DE TOROS',
      imagenURL:'https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Albero-lasventas.jpg/375px-Albero-lasventas.jpg',
      coments: ['AWESOME','Incredibolt']
    },
    {
      id:'3',
      title:'PLAZA DE TOROS',
      imagenURL:'https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Albero-lasventas.jpg/375px-Albero-lasventas.jpg',
      coments: []
    }
  ]
  constructor() { }
  getPlaces(){
    return [...this.places]//devuelve todos los palces
  }
  getPlace(placeId:string){
    return{
      ...this.places.find(place =>{ //busca y devuelve el melemento que tenga el mismo id
        return place.id ===placeId
      }
        )
    }
  }
  addPlace(title,imagenURL){
      this.places.push({
        title,
        imagenURL,
        coments:[],
        id:this.places.length +1+""//añade uno y lo suma
      })
  }
  deletePlace(placeID:string){
    this.places=this.places.filter(place =>{
      return place.id !==placeID
    })
  }
  
}
