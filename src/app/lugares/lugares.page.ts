import { Component, OnInit } from '@angular/core';
import {LugaresService} from './lugares.service'
import {Router} from '@angular/router'
@Component({
  selector: 'app-lugares',
  templateUrl: './lugares.page.html',
  styleUrls: ['./lugares.page.scss'],
})
export class LugaresPage implements OnInit {
  places=[]
  constructor(private lugaresService: LugaresService,private router:Router) { }

  ngOnInit() {//Este solo se ejecuta la primera vez
    this.places=this.lugaresService.getPlaces()
  }
  ionViewWillEnter(){
    this.places=this.lugaresService.getPlaces()
  }
  addNuevoLugar(){
    this.router.navigate(['/nuevo-lugares'])
  }
  gotoHome(){
    this.router.navigate(['/home'])
  }
}
