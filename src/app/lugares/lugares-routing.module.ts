import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LugaresPage } from './lugares.page';

const routes: Routes = [
  {
    path: '',
    component: LugaresPage
  },
  {
    path: 'lugares-detalle',
    loadChildren: () => import('./lugares-detalle/lugares-detalle.module').then( m => m.LugaresDetallePageModule)
  },
  {
    path: 'lugares-add',
    loadChildren: () => import('./lugares-add/lugares-add.module').then( m => m.LugaresAddPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LugaresPageRoutingModule {}
